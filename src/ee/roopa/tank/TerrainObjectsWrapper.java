package ee.roopa.tank;

public class TerrainObjectsWrapper {
	private int x;
	private int y;
	private String terrainChar;
	public TerrainObjectsWrapper(int x, int y, String terrainChar) {
		this.x = x;
		this.y = y;
		this.terrainChar = terrainChar;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public String getTerrainChar() {
		return terrainChar;
	}
}
