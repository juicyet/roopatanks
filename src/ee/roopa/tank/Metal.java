package ee.roopa.tank;


import android.graphics.drawable.BitmapDrawable;

public class Metal extends GameObject {

	public Metal(BitmapDrawable drawable , int x, int y, int width, int height) {
		super(drawable, x, y, width, height, 0);
	}
}
