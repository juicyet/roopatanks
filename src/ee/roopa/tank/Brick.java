package ee.roopa.tank;


import android.graphics.drawable.BitmapDrawable;

public class Brick extends GameObject {

	public Brick(BitmapDrawable drawable , int x, int y, int width, int height) {
		super(drawable, x, y, width, height, 0);
	}
}
