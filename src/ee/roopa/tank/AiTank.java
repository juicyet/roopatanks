package ee.roopa.tank;

import java.util.Random;

import android.graphics.drawable.BitmapDrawable;


public class AiTank extends Tank {
	private int smartness;
	public int enemyShield = 0;
	
	// TODO These should include all parameters a la smartness, shield, lives, speed etc
	public static enum AiTankType { 
		NORMAL, 
		BADASS, 
		FAST,
		BOSS
	};
	public static enum AiTankStrategy { 
		COWARD, 
		ASSAULT,
		DEFENDER,
		CAMPER
	};
	
	public AiTank(BitmapDrawable drawable, int x, int y, int width, int height, int maxSpeed, int smartness) {
		super(drawable, x, y, width, height, maxSpeed);
		this.currentSpeed = maxSpeed;
		this.smartness = smartness;
	}
	
	@Override
	public void update() {
		
		super.update();		
		Random r = new Random();
		
		//dumb AiTank
		if(this.smartness == 1) {
			
			switch(r.nextInt(50)) {
				case 0:
					this.setDirection(Direction.DOWN);
					currentSpeed = maxSpeed;
					break;
				case 1:
					this.setDirection(Direction.RIGHT);
					currentSpeed = maxSpeed;
					break;
				case 2:
					this.setDirection(Direction.LEFT);
					currentSpeed = maxSpeed;
					break;
				case 3:
					this.setDirection(Direction.UP);
					currentSpeed = maxSpeed;
					break;
				case 4:
					currentSpeed = 0;
					break;
				default: break;
			}
			
			//Must NOT be done before changing direction. Otherwise immediate collision with its own bullet!
			if(r.nextInt(20) == 0) {
				super.shoot();
			}
		}
		// sergeant AiTank
			
		//TODO must see walls somehow
		else if(this.smartness == 2) {
			switch(r.nextInt(6)){
				case 0: case 1: this.setDirection(Direction.DOWN); 	break;
				case 2: 		this.setDirection(Direction.RIGHT); break;
				case 3: 		this.setDirection(Direction.LEFT); 	break;
				case 4: 		this.setDirection(Direction.UP); 	break;
				default: break;
			}
		}
		// elite AiTank
		
		//TODO must see walls and bullets somehow
		else if(this.smartness == 3) {
		
		}
	}
	
	private boolean seeWalls(Direction direction, int x, int y) {
		return false;
	}
	private boolean seeBullets(Direction direction, int x, int y) {
		return false;
	}
}
