package ee.roopa.tank;

public class NewBulletEvent {

	private Bullet bullet;

	public Bullet getBullet() {
		return bullet;
	}

	public void setBullet(Bullet bullet) {
		this.bullet = bullet;
	}

}
