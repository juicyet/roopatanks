package ee.roopa.tank;


import android.graphics.drawable.BitmapDrawable;

public class Grass extends GameObject {

	public Grass(BitmapDrawable drawable , int x, int y, int width, int height) {
		super(drawable, x, y, width, height, 0);
	}
}
