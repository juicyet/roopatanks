/**
 * 
 */
package ee.roopa.tank;

import android.graphics.drawable.BitmapDrawable;

/**
 * Game Object interface that guaranties that every GO needs to implement these methods
 * 
 * @author andrelv
 *
 */
public interface IGameObject 
{
	public BitmapDrawable getObjectImage();

	public void setObjectImage(BitmapDrawable objectImage);
}
