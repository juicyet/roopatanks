package ee.roopa.tank;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import roopa.ee.R;


public enum PowerUpType {
	NO_POWERUP (75, 0), 
	TANK_SHIELD (5, R.drawable.tank_shield), 
	ENEMY_TIME_STOP (5, R.drawable.enemy_time_stop), 
	WEAPON_DUALBULLET (5, R.drawable.weapon_dualbullet), 
	WEAPON_SINGLEBULLET_IRONEATER (5, R.drawable.weapon_singlebullet_ironeater), 
	WEAPON_SINGLEBULLET_ALLEATER (5, R.drawable.weapon_singlebullet_alleater);
	
	private final int dropchange;
	public int getDropchange() {
		return dropchange;
	}

	private final int drawable;	
	public int getDrawable() {
		return drawable;
	}

	PowerUpType(int dropchange, int drawable) {
	    this.dropchange = dropchange;
	    this.drawable = drawable;
	}
	
	private static final List<PowerUpType> VALUES = Collections.unmodifiableList(Arrays.asList(values()));
	private static final int SIZE = summWeigts();
	private static final Random RANDOM = new Random();

	private static int summWeigts() {
		int summ = 0;
		for(PowerUpType value: VALUES) {
	    	summ += value.getDropchange();
		}
		return summ;
	}
	
    public static PowerUpType randomPowerup()  {
        int randomNum = RANDOM.nextInt(SIZE);
        int currentWeightSumm = 0;
        for(PowerUpType currentValue: VALUES) {
           if (randomNum > currentWeightSumm && randomNum <= (currentWeightSumm + currentValue.getDropchange())) {
        	   return currentValue;
           }
           currentWeightSumm += currentValue.getDropchange();
        }

        return null;
    }	
}
