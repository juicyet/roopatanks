package ee.roopa.tank;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

public class Bullet {
	private int x, y; 
	private int speed = Properties.BULLET_SPEED;
	private int radius = Properties.BULLET_RADIUS_NORMAL;
	private Teams owner;
	private Direction direction;
	private BulletType weaponType;
	Paint paint;
	
	public static enum BulletType { 
		WEAPON_SINGLEBULLET, 
		WEAPON_DUALBULLET, 
		WEAPON_SINGLEBULLET_IRONEATER, 
		WEAPON_SINGLEBULLET_ALLEATER
	};
	
	public Bullet(Teams owner, int x, int y, Direction direction) {
		this(owner, x, y, direction, BulletType.WEAPON_SINGLEBULLET);
	}

	public Bullet(Teams owner, int x, int y, Direction direction, BulletType type) {
		this.owner = owner;
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.weaponType = type;
		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setColor(Color.WHITE);		
	}
	
	public void show(Canvas canvas) {
		canvas.drawCircle(x, y, radius, paint);		
	}
	
	public BulletType getWeaponType() {
		return weaponType;
	}

	public void setWeaponType(BulletType weaponType) {
		this.weaponType = weaponType;
	}
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getRadius() {
		return radius;
	}

	public void update() {
		if(direction == Direction.LEFT) {
			x -= speed;
		}
		else if(direction == Direction.RIGHT) {
			x += speed;
		}
		else if(direction == Direction.UP) {
			y -= speed;
		}
		else if(direction == Direction.DOWN) {
			y += speed;
		}
	}
	public boolean isOutOfArea() 
	{	
		// left border check
		if(this.x <= Properties.PADDING && this.direction == Direction.LEFT) {
			return true;
		}
		// right border check
		if(this.x + this.radius >= Properties.GAME_AREA_WIDTH - Properties.PADDING && this.direction == Direction.RIGHT) {
			return true;
		}
		
		if(this.y <= Properties.PADDING && this.direction == Direction.UP) {
			return true;
		}
		if(this.y + this.radius >= Properties.GAME_AREA_HEIGHT - Properties.PADDING && this.direction == Direction.DOWN) {
			return true;
		}
		return false;
	}
	
	
}
