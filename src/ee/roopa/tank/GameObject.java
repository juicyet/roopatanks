package ee.roopa.tank;

import java.util.Observable;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

public abstract class GameObject extends Observable {

	protected int x, y, width, height;
	protected int currentSpeed;
	protected int maxSpeed;
	protected Direction direction = Direction.RIGHT;
	private BitmapDrawable objectImage;
	
	public GameObject(BitmapDrawable objectImage, int x, int y, int width, int height, int maxSpeed) {
		this.objectImage = objectImage;
		this.currentSpeed = 0;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.maxSpeed = maxSpeed;
	}

	public int getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(int currentSpeed) {
		this.currentSpeed = currentSpeed;
	}

	public int getMaxSpeed() {
		return maxSpeed;
	}
	
	public BitmapDrawable getObjectImage() {
		return objectImage;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public Direction getDirection() {
		return direction;
	}

	public void update() {	
		if (this.checkOutOfArea()) {
			currentSpeed = 0;
		}
		if(this.direction == Direction.UP) {
			y -= currentSpeed;
		}
		else if(this.direction == Direction.RIGHT) {
			x += currentSpeed;		
		}
		else if(this.direction == Direction.LEFT) {
			x -= currentSpeed;
		}
		else {
			y += currentSpeed;
		}
		
	}
	// Todo that could be in local update if we could get getWidth() somehow here
	public boolean checkOutOfArea() 
	{	
		// left border check
		if(this.x <= Properties.PADDING && this.direction == Direction.LEFT) {
			return true;
		}
		// right border check
		if(this.x + this.width >= Properties.GAME_AREA_WIDTH - Properties.PADDING && this.direction == Direction.RIGHT) {
			return true;
		}
		
		if(this.y <= Properties.PADDING && this.direction == Direction.UP) {
			return true;
		}
		if(this.y + this.height >= Properties.GAME_AREA_HEIGHT - Properties.PADDING && this.direction == Direction.DOWN) {
			return true;
		}
		return false;
	}
	
	public void show(Canvas canvas) {
		objectImage.setBounds(x, y, x + width, y + height);
		objectImage.draw(canvas);
	}
	
	public boolean intersect(GameObject wall) {
		Rect tankRect = new Rect(this.getX(), this.getY(), this.getX() + this.getWidth(), this.getY() + this.getHeight());
		Rect wallRect = new Rect(wall.getX(), wall.getY(), wall.getX() + wall.getWidth(), wall.getY() + wall.getHeight());
		if(tankRect.intersect(wallRect)) {
			if(this.x < wall.getX() + wall.getWidth() && this.direction == Direction.LEFT) {
				return true;
			}
			if(this.x + this.width < wall.x && this.direction == Direction.RIGHT) {
				return true;
			}
			if(this.y + this.height > wall.getY() && this.direction == Direction.DOWN) {
				return true;
			}
			if(this.y < wall.getY() + wall.getHeight() && this.direction == Direction.UP) {
				return true;
			}
		}
		
		/*
		if(((this.x < wall.getX() + wall.getWidth() && this.direction == Direction.LEFT) &&
				(this.x + this.width < wall.x && this.direction == Direction.RIGHT)) ||
			((this.y + this.height > wall.getY() && this.direction == Direction.DOWN) &&
				(this.y < wall.getY() + wall.getHeight() && this.direction == Direction.UP))) {
			return true;
		}*/
		
		
		return false;
		
		
	}
	
	public boolean intersect(Bullet bullet) {
		if(bullet.getX() - bullet.getRadius() < this.x + this.width 
				&& bullet.getX() + bullet.getRadius() > this.x 
				&& bullet.getY()  + bullet.getRadius() > this.y 
				&& bullet.getY() - bullet.getRadius() < this.y + this.height) {
			return true;
		}
		return false;
	}
	
	
	
	
	private void setImageDirection(Direction toDirection) {
		
		Bitmap bitmapOrg = this.objectImage.getBitmap();
        int width = bitmapOrg.getWidth();
        int height = bitmapOrg.getHeight();
		
        // rotate the current image
		Matrix matrix = new Matrix();
		matrix.setRotate(Direction.getRotationDegree(this.direction, toDirection));
		
 		Log.d("Tank direction", direction.toString() + ' ' + this.direction + ' ' + toDirection);
 		Log.d("Tank image orig", "W " + Integer.toString(width) + "H " + Integer.toString(height));
		
        // recreate the new Bitmap
		Bitmap tmpBitmap = Bitmap.createBitmap(bitmapOrg, 0, 0, width, height, matrix, true);
		this.objectImage = new BitmapDrawable(tmpBitmap);
	}
	

	
	public void setDirection(Direction direction) {
		this.setImageDirection(direction);
		this.direction = direction;
	}
}
