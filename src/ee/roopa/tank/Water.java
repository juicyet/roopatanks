package ee.roopa.tank;


import android.graphics.drawable.BitmapDrawable;

public class Water extends GameObject {

	public Water(BitmapDrawable drawable , int x, int y, int width, int height) {
		super(drawable, x, y, width, height, 0);
	}
}
