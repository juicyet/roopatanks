package ee.roopa.tank;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import roopa.ee.R;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class GameView extends View implements OnTouchListener, Observer {
		
	private RefreshHandler mRedrawHandler = new RefreshHandler();
	private Context mContext;
	Resources resources;
	
	private int myIndexInTanksList = 0;
	
	private List<Bullet> bullets = new ArrayList<Bullet>();
	private List<GameObject> gameObjects = new ArrayList<GameObject>();
	
	private long counterSpawnTanks = 0;
	private int TANK_SPAWN_FREQUENCY = 100;
	private GameState gameState;
	private GameOverText gameOverText;
	
	Drawable arrowsPanel;
	ArrowPressedPanel arrowPressedPanel;
	Drawable shootButton;
	Drawable shootButtonAura;
	
	public GameView(Context context) {
 		super(context);
		mContext = context;
		resources = mContext.getResources();
		gameState = GameState.GAME_RUNNING;

		this.setOnTouchListener(this);		
		
		Log.d("Size", Integer.toString(getWidth()) + ' ' + Integer.toString(getHeight()));
		//SocketConnection connection = new SocketConnection();

	}	
	
	@Override
	protected void onDraw(Canvas canvas) {
		for (GameObject obj : gameObjects) {
			obj.show(canvas);
		}
		
		for (Bullet bullet : bullets) {
			bullet.show(canvas);
		}
		
		arrowsPanel.setBounds(
				Properties.ARROW_CONTROL_X, 
				Properties.ARROW_CONTROL_Y,  
				Properties.ARROW_CONTROL_X + Properties.ARROW_CONTROL_WIDTH,
				Properties.ARROW_CONTROL_Y + Properties.ARROW_CONTROL_HEIGHT);
		arrowsPanel.draw(canvas);
		
		arrowPressedPanel.show(canvas);
		
		
		shootButton.setBounds(
				Properties.SHOOT_BUTTON_X, 
				Properties.SHOOT_BUTTON_Y,  
				Properties.SHOOT_BUTTON_X + Properties.SHOOT_BUTTON_WIDTH,
				Properties.SHOOT_BUTTON_Y + Properties.SHOOT_BUTTON_HEIGHT);
		shootButton.draw(canvas);
		
		if (gameState == GameState.GAME_OVER) {
			gameOverText.show(canvas);
		}		
		shootButton.draw(canvas);	
		
		shootButtonAura.setBounds(
				Properties.SHOOT_BUTTON_X, 
				Properties.SHOOT_BUTTON_Y,  
				Properties.SHOOT_BUTTON_X + Properties.SHOOT_BUTTON_WIDTH,
				Properties.SHOOT_BUTTON_Y + Properties.SHOOT_BUTTON_HEIGHT);
		shootButtonAura.draw(canvas);
	};

	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		initStage();
		invalidate();
		update();

	};
	
	private void initStage() {
		if (getHeight() == 0 || getWidth() == 0)
			throw new RuntimeException("This method is called at wrong time. Environment height and width are expected to be available at this point");
		
		Properties.GAME_AREA_HEIGHT = getHeight();
		Properties.GAME_AREA_WIDTH = getWidth();
		
		// initialize spawning points with proper coordinates
		Properties.SPAWNING_POINT_LEFT_UPPER = new Point(20, Properties.GAME_AREA_HEIGHT/3);
		Properties.SPAWNING_POINT_LEFT_LOWER = new Point(20, Properties.GAME_AREA_HEIGHT*2/3);
		Properties.SPAWNING_POINT_RIGHT_UPPER = new Point(Properties.GAME_AREA_WIDTH - 70, Properties.GAME_AREA_HEIGHT/3);
		Properties.SPAWNING_POINT_RIGHT_LOWER = new Point(Properties.GAME_AREA_WIDTH - 70, Properties.GAME_AREA_HEIGHT*2/3);
		
		Properties.SPAWNING_POINT_TOP_LEFT = new Point(Properties.GAME_AREA_WIDTH  / 2 - Properties.TURKEY_WIDTH / 2 - Properties.TANK_WIDTH_NORMAL - 20, 20 + Properties.GAMEOBJECT_FATNESS);
		Properties.SPAWNING_POINT_TOP_RIGHT = new Point(Properties.GAME_AREA_WIDTH  / 2 + Properties.TURKEY_WIDTH / 2 + Properties.GAMEOBJECT_FATNESS, 20 + Properties.GAMEOBJECT_FATNESS);
		Properties.SPAWNING_POINT_BOTTOM_LEFT = new Point(Properties.GAME_AREA_WIDTH  / 2 - Properties.TURKEY_WIDTH / 2 - Properties.TANK_WIDTH_NORMAL - 20, Properties.GAME_AREA_HEIGHT - Properties.TANK_WIDTH_NORMAL - Properties.GAMEOBJECT_FATNESS);
		Properties.SPAWNING_POINT_BOTTOM_RIGHT = new Point(Properties.GAME_AREA_WIDTH  / 2 + Properties.TURKEY_WIDTH / 2 + Properties.GAMEOBJECT_FATNESS, Properties.GAME_AREA_HEIGHT - Properties.TANK_WIDTH_NORMAL - Properties.GAMEOBJECT_FATNESS);
		
		Properties.ARROW_CONTROL_X = 10;
		Properties.ARROW_CONTROL_Y = Properties.GAME_AREA_HEIGHT - Properties.ARROW_CONTROL_HEIGHT;
		
		Properties.SHOOT_BUTTON_X = Properties.GAME_AREA_WIDTH - Properties.SHOOT_BUTTON_WIDTH;
		Properties.SHOOT_BUTTON_Y = Properties.GAME_AREA_HEIGHT - Properties.SHOOT_BUTTON_HEIGHT;
		
		Properties.TURKEY_X_LOWER = Properties.GAME_AREA_WIDTH  / 2 - Properties.TURKEY_WIDTH / 2;
		Properties.TURKEY_Y_LOWER = Properties.GAME_AREA_HEIGHT - Properties.TURKEY_HEIGHT;
		
		Properties.TURKEY_X_UPPER = Properties.TURKEY_X_LOWER = Properties.GAME_AREA_WIDTH  / 2 - Properties.TURKEY_WIDTH / 2;
		Properties.TURKEY_Y_UPPER = 0;
		
		arrowsPanel = (Drawable)resources.getDrawable(R.drawable.arrows);
		arrowsPanel.setAlpha(Properties.CONTROLS_OPACITY);
		
		arrowPressedPanel = new ArrowPressedPanel(
				(BitmapDrawable)resources.getDrawable(R.drawable.arrow_clicked),
				Properties.ARROW_CONTROL_X, 
				Properties.ARROW_CONTROL_Y,  
				Properties.ARROW_CONTROL_WIDTH,
				Properties.ARROW_CONTROL_HEIGHT,
				0);		
		
		shootButton = (Drawable)resources.getDrawable(R.drawable.shoot);
		shootButton.setAlpha(Properties.CONTROLS_OPACITY);
		
		shootButtonAura = (Drawable)resources.getDrawable(R.drawable.shoot_clicked);
		shootButtonAura.setAlpha(0);
		
		
		//---------------------------------------------------------------
		// player tankide spawnimine (tahan n�ha, et spawn punktide koordinaadid oleksid ok)
		
		Tank playerTank = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank), 
				Properties.SPAWNING_POINT_BOTTOM_RIGHT.x, 
				Properties.SPAWNING_POINT_BOTTOM_RIGHT.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				Properties.TANK_SPEED_NORMAL);
		playerTank.setDirection(Direction.UP);
		playerTank.addObserver(this);
		gameObjects.add(playerTank);
		/*
		Tank playerTank2 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank), 
				Properties.SPAWNING_POINT_BOTTOM_LEFT.x, 
				Properties.SPAWNING_POINT_BOTTOM_LEFT.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				Properties.TANK_SPEED_NORMAL);
		playerTank2.setDirection(Direction.UP);
		playerTank2.addObserver(this);
		gameObjects.add(playerTank2);
		
		Tank playerTank3 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank), 
				Properties.SPAWNING_POINT_TOP_RIGHT.x, 
				Properties.SPAWNING_POINT_TOP_RIGHT.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				Properties.TANK_SPEED_NORMAL);
		playerTank3.setDirection(Direction.DOWN);
		playerTank3.addObserver(this);
		gameObjects.add(playerTank3);
		
		Tank playerTank4 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank), 
				Properties.SPAWNING_POINT_TOP_LEFT.x, 
				Properties.SPAWNING_POINT_TOP_LEFT.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				Properties.TANK_SPEED_NORMAL);
		playerTank4.setDirection(Direction.DOWN);
		playerTank4.addObserver(this);
		gameObjects.add(playerTank4);
		//---------------------------------------------------------------
		
		
		// test ai tank spawn points
		Tank testTank1 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank_green), 
				Properties.SPAWNING_POINT_LEFT_LOWER.x, 
				Properties.SPAWNING_POINT_LEFT_LOWER.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				1);
		testTank1.setDirection(Direction.RIGHT);
		testTank1.addObserver(this);
		gameObjects.add(testTank1);
		
		Tank testTank2 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank_green), 
				Properties.SPAWNING_POINT_LEFT_UPPER.x, 
				Properties.SPAWNING_POINT_LEFT_UPPER.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				1);
		testTank2.setDirection(Direction.RIGHT);
		testTank2.addObserver(this);
		gameObjects.add(testTank2);
		
		Tank testTank3 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank_green), 
				Properties.SPAWNING_POINT_RIGHT_UPPER.x, 
				Properties.SPAWNING_POINT_RIGHT_UPPER.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				1);
		testTank3.setDirection(Direction.LEFT);
		testTank3.addObserver(this);
		gameObjects.add(testTank3);
		
		Tank testTank4 = new Tank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank_green), 
				Properties.SPAWNING_POINT_RIGHT_LOWER.x, 
				Properties.SPAWNING_POINT_RIGHT_LOWER.y,
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_HEIGHT_NORMAL,
				1);
		testTank4.setDirection(Direction.LEFT);
		testTank4.addObserver(this);
		gameObjects.add(testTank4);
		*/
		gameOverText = new GameOverText(
				(BitmapDrawable)resources.getDrawable(R.drawable.gameover), 
				Properties.GAME_AREA_WIDTH / 2 - Properties.GAME_OVER_TEXT_WITH / 2,
				Properties.GAME_AREA_HEIGHT - Properties.GAME_OVER_TEXT_HEIGHT -1, 				
				Properties.GAME_OVER_TEXT_WITH,
				Properties.GAME_OVER_TEXT_HEIGHT,
				1);
		gameOverText.setDirection(Direction.UP);
		
		Turkey turkeyLower = new Turkey((BitmapDrawable)resources.getDrawable(R.drawable.kalkun), Properties.TURKEY_X_LOWER, Properties.TURKEY_Y_LOWER, Properties.TURKEY_WIDTH, Properties.TURKEY_HEIGHT);
		gameObjects.add(turkeyLower);
		
		Turkey turkeyUpper = new Turkey((BitmapDrawable)resources.getDrawable(R.drawable.kalkun), Properties.TURKEY_X_UPPER, Properties.TURKEY_Y_UPPER, Properties.TURKEY_WIDTH, Properties.TURKEY_HEIGHT);
		gameObjects.add(turkeyUpper);
		
		
		
		BufferedReader reader = null;
		InputStream is = this.getResources().openRawResource
				(R.drawable.iisi);
//				(R.drawable.level1);
//	            (R.drawable.muruplats);
				
		try {
			reader = new BufferedReader(new InputStreamReader(is));
		}
		catch (Exception e) {
			System.out.println("No CSV file found");
		}
		List<TerrainObjectsWrapper> gameMap = MapReader.readMap(reader, resources);
		Properties.GAMEOBJECT_FATNESS = (int) Math.floor(Properties.GAME_AREA_HEIGHT / Properties.mapHeight);
		int startFrom = (Properties.GAME_AREA_WIDTH - (Properties.GAMEOBJECT_FATNESS * Properties.mapHeight)) / 2;		
		for (TerrainObjectsWrapper tow : gameMap) {
			// TODO: Make it smaller
			String terrainChar = tow.getTerrainChar();
			GameObject gameObject = null;
			if (terrainChar.equalsIgnoreCase(Properties.BRICK_CHAR)) {
				gameObject = new Brick((BitmapDrawable)resources.getDrawable(R.drawable.brick), startFrom + tow.getX() * Properties.GAMEOBJECT_FATNESS, tow.getY() * Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS);
			}
			else if (terrainChar.equalsIgnoreCase(Properties.METAL_CHAR)) {
				gameObject = new Metal((BitmapDrawable)resources.getDrawable(R.drawable.metal), startFrom + tow.getX() * Properties.GAMEOBJECT_FATNESS, tow.getY() * Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS);
			}
			else if (terrainChar.equalsIgnoreCase(Properties.WATER_CHAR)) {
				gameObject = new Water((BitmapDrawable)resources.getDrawable(R.drawable.water), startFrom + tow.getX() * Properties.GAMEOBJECT_FATNESS, tow.getY() * Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS);
			}
			else if (terrainChar.equalsIgnoreCase(Properties.GRASS_CHAR)) {
				gameObject = new Bush((BitmapDrawable)resources.getDrawable(R.drawable.grass), startFrom + tow.getX() * Properties.GAMEOBJECT_FATNESS, tow.getY() * Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS, Properties.GAMEOBJECT_FATNESS);
			}
			else if (terrainChar.equalsIgnoreCase(Properties.EMPTY_CHAR)) {
			}
			else {
				throw(new RuntimeException("INVALID TOKEN"));
			}
			if(gameObject != null){
				gameObjects.add(gameObject);
			}
		}
//		for (int i = 0; i<10; i++) {
//			spawnAiTank();
//		}
	}

	private void spawnAiTank() {
		
		Point spawnPoint = null;
		Direction spawnDirection;
		Random r = new Random();
		switch(r.nextInt(4)) {
			case 0:
				spawnPoint = Properties.SPAWNING_POINT_LEFT_LOWER;
				spawnDirection = Direction.RIGHT;
				break;			
			case 1: 
				spawnPoint = Properties.SPAWNING_POINT_LEFT_UPPER;
				spawnDirection = Direction.RIGHT;
				break;
			case 2: 
				spawnPoint = Properties.SPAWNING_POINT_RIGHT_LOWER;
				spawnDirection = Direction.LEFT;
				break;
			case 3: 
				spawnPoint = Properties.SPAWNING_POINT_RIGHT_UPPER;
				spawnDirection = Direction.LEFT;
				break;
			default: throw new RuntimeException("Kesse andis tanki spawnimise randomile vale max numbri parameetriks :)");
		}
		
		AiTank aiTank = new AiTank(
				(BitmapDrawable)resources.getDrawable(R.drawable.tank), 
				spawnPoint.x, 
				spawnPoint.y,
				Properties.TANK_HEIGHT_NORMAL, 
				Properties.TANK_WIDTH_NORMAL,
				Properties.TANK_SPEED_NORMAL, 
				1);
		aiTank.setDirection(spawnDirection);
		aiTank.addObserver(this);
		gameObjects.add(aiTank);
	}
	
	// need 4 booleanit on hetkel allavajutatud nuppude kaardistamiseks
	boolean leftArrowIsDown = false;
	boolean rightArrowIsDown = false;
	boolean upArrowIsDown = false;
	boolean downArrowIsDown = false;
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		
		if(keyCode == KeyEvent.KEYCODE_MENU 
				|| keyCode == KeyEvent.KEYCODE_BACK 
				|| keyCode == KeyEvent.KEYCODE_HOME) {
		}
		return super.onKeyDown(keyCode, event);	
	}
	
	public boolean onTouch(View v, MotionEvent event) {
		if(gameState == GameState.GAME_OVER) { 
			return false;
		}
		float touchX = event.getX();
		float touchY = event.getY();
		int eventaction = event.getAction();
		Tank tank = (Tank) gameObjects.get(myIndexInTanksList);
		
		if(touchX > Properties.GAME_AREA_WIDTH - Properties.SHOOT_BUTTON_WIDTH && touchY > Properties.GAME_AREA_HEIGHT - Properties.SHOOT_BUTTON_HEIGHT) {
			tank.shoot();
			shootButtonAura.setAlpha(Properties.CONTROLS_OPACITY);
		}
		
		switch (eventaction)
		{
			case MotionEvent.ACTION_DOWN: // touch on the screen event				
				//shootButtonControl.doShoot(nTank, dX, dY);
			case MotionEvent.ACTION_MOVE: // move event
				if (touchX < Properties.ARROW_CONTROL_WIDTH && touchY > Properties.GAME_AREA_HEIGHT - Properties.ARROW_CONTROL_HEIGHT) {			
					if (touchX + Properties.GAME_AREA_HEIGHT - Properties.ARROW_CONTROL_HEIGHT < touchY) {
						if (touchX < Properties.GAME_AREA_HEIGHT - touchY) {
							arrowPressedPanel.setDirection(Direction.LEFT);
							arrowPressedPanel.getObjectImage().setAlpha(Properties.CONTROLS_OPACITY);
							tank.setDirection(Direction.LEFT);
							tank.setCurrentSpeed(tank.getMaxSpeed());
						}
						else {
							arrowPressedPanel.setDirection(Direction.DOWN);
							arrowPressedPanel.getObjectImage().setAlpha(Properties.CONTROLS_OPACITY);
							tank.setDirection(Direction.DOWN);							
							tank.setCurrentSpeed(tank.getMaxSpeed());
						}
					}
					else if (touchX + Properties.GAME_AREA_HEIGHT - Properties.ARROW_CONTROL_HEIGHT > touchY) {
						if (touchX < Properties.GAME_AREA_HEIGHT - touchY) {
							arrowPressedPanel.setDirection(Direction.UP);
							arrowPressedPanel.getObjectImage().setAlpha(Properties.CONTROLS_OPACITY);
							tank.setDirection(Direction.UP);
							tank.setCurrentSpeed(tank.getMaxSpeed());
						}
						else {
							arrowPressedPanel.setDirection(Direction.RIGHT);
							arrowPressedPanel.getObjectImage().setAlpha(Properties.CONTROLS_OPACITY);
							tank.setDirection(Direction.RIGHT);
							tank.setCurrentSpeed(tank.getMaxSpeed());
						}
					}
				}
				break;
			case MotionEvent.ACTION_UP:  // finger up event
				tank.setCurrentSpeed(0);
				arrowPressedPanel.getObjectImage().setAlpha(0);
				shootButtonAura.setAlpha(0);
			    break;
			
		}		
		return true;
	}

	private class RefreshHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			GameView.this.update();
			GameView.this.invalidate();

		}

		public void sleep(long delayMillis) {
			this.removeMessages(0);
			sendMessageDelayed(obtainMessage(0), delayMillis);
		}
	}

	public void update() {
		//This has to be done before updating gameobject and bullet coordinates!
		tankEnvironmentCollision();
		bulletCollison();		
		if (gameState == GameState.GAME_OVER) {
			gameOverText.update();
		}
		
		for (GameObject gameObject : gameObjects) {
			gameObject.update();	
		}
		for (Bullet bullet : bullets) {
			bullet.update();
		}
			
		//spawn tanks
		if (counterSpawnTanks > TANK_SPAWN_FREQUENCY){
			counterSpawnTanks = 0;
			spawnAiTank();
		}		
		mRedrawHandler.sleep(40);
	}
	
	private void tankEnvironmentCollision() {
		for (GameObject tank : gameObjects) {
			if (tank instanceof Tank) {
				for (GameObject wall : gameObjects) {
					if (!tank.equals(wall) && tank.intersect(wall)) {
						tank.setCurrentSpeed(0);
					}
				}
			}
		}
	}
	
	private void bulletCollison() {
		
		ArrayList<Bullet> deadBullets = new ArrayList<Bullet>();
		for (Bullet bullet : bullets) {		
			if(bullet.isOutOfArea()) {
				deadBullets.add(bullet);
				continue;
			}
			for (Bullet bulletSub : bullets) {
				if(!bullet.equals(bulletSub) && 
						Math.abs((double)(bullet.getX() - bulletSub.getX())) < (bullet.getRadius() + bulletSub.getRadius()) &&
						Math.abs((double)(bullet.getY() - bulletSub.getY())) < (bullet.getRadius() + bulletSub.getRadius())) {
					deadBullets.add(bullet);
					deadBullets.add(bulletSub);
					break;
				}
			}
		}
			
		for (Bullet bullet : deadBullets) {	
			bullets.remove(bullet);
		}
		
		for (Iterator<Bullet> bulletIter = bullets.iterator(); bulletIter.hasNext();) {
			Bullet bullet = bulletIter.next();
			
			for (Iterator<GameObject> gameObjectIter = gameObjects.iterator(); gameObjectIter.hasNext();) {
				GameObject gameObject = gameObjectIter.next();
				if (gameObject.intersect(bullet)) {
					if(gameObject.equals(gameObjects.get(myIndexInTanksList))) {
						gameState = GameState.GAME_OVER;
					}
					gameObjectIter.remove();
					bulletIter.remove();
					break;
				}
			}
		}
	}
	
	
	public void update(Observable observable, Object data) {
		if (data instanceof NewBulletEvent) {
			Bullet newBullet = ((NewBulletEvent)data).getBullet();
			bullets.add(newBullet);
		}
	}
}
