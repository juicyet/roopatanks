package ee.roopa.tank;

import android.graphics.drawable.BitmapDrawable;

public class ArrowPressedPanel extends GameObject {

	public ArrowPressedPanel(BitmapDrawable objectImage, int x, int y, int width, int height, int maxSpeed) {
		super(objectImage, x, y, width, height, maxSpeed);	
		getObjectImage().setAlpha(0);
	}

}
