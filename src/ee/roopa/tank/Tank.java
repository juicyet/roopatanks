package ee.roopa.tank;


import ee.roopa.tank.Bullet.BulletType;
import android.graphics.drawable.BitmapDrawable;

public class Tank extends GameObject {
	
	private Teams team;	
	protected BulletType bulletType = BulletType.WEAPON_SINGLEBULLET;
	private int cooldown = 0;

	public Tank(BitmapDrawable drawable, int x, int y, int width, int height, int maxSpeed) {
		super(drawable, x, y, width, height, maxSpeed);
		this.direction = Direction.RIGHT;
	}
	public void update() {
		super.update();
		cooldown++;		
	}
	
	public void shoot() {
		if(!(cooldown >= Properties.BULLET_COOLDOWN_COUNTER)) {
			return;
		}
		
		cooldown = 0;		
		int bulletX = 0, bulletY = 0;
		switch (direction) {
		case UP: 
			bulletX = x + width / 2; 
			bulletY = y - Properties.BULLET_RADIUS_NORMAL - 1; 
			break;
		case DOWN: 
			bulletX = x + width / 2;
			bulletY = y + height + Properties.BULLET_RADIUS_NORMAL + 1;
			break;
		case LEFT: 
			bulletX = x - Properties.BULLET_RADIUS_NORMAL - 1;
			bulletY = y + height / 2;
			break;
		case RIGHT: 
			bulletX = x + width + Properties.BULLET_RADIUS_NORMAL + 1;
			bulletY = y + height / 2;
			break;
		}

		Bullet bullet = new Bullet(team, bulletX, bulletY, direction, this.bulletType);
		//bullets.add(bullet);
		NewBulletEvent event = new NewBulletEvent();
		event.setBullet(bullet);
		setChanged();
		notifyObservers(event);
	}

	public Teams getTeam() {
		return team;
	}

	public void setTeam(Teams team) {
		this.team = team;
	}
	
	public BulletType getBulletType() {
		return bulletType;
	}

	public void setBulletType(BulletType bulletType) {
		this.bulletType = bulletType;
	}
	
}
