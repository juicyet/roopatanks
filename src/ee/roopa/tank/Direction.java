package ee.roopa.tank;

public enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN;
	
    private static final float HALF_ROTATION_DEGREE = 180.0f;
    private static final float NULL_ROTATION_DEGREE = 0.0f; 
    private static final float THREE_QUARTER__ROTATION_DEGREE = 270.0f;
    private static final float QUARTER_ROTATION_DEGREE = 90.0f; 
			
	/**
	 * Returns the rotation degree based from the original and to direction
	 * @param fromDirection - Direction the rotation starts
	 * @param toDirection - Direction the rotation is set for
	 * @return
	 */
    public static float getRotationDegree(Direction fromDirection, Direction toDirection) {
		switch (fromDirection) {
			case LEFT:
				switch (toDirection) {
					case LEFT:
						return NULL_ROTATION_DEGREE;
					case RIGHT:
						return HALF_ROTATION_DEGREE;
					case UP:
						return QUARTER_ROTATION_DEGREE;
					case DOWN:
						return THREE_QUARTER__ROTATION_DEGREE;
					default:
						return NULL_ROTATION_DEGREE;
				}
			case RIGHT:
				switch (toDirection) {
				case LEFT:
					return HALF_ROTATION_DEGREE;
				case RIGHT:
					return NULL_ROTATION_DEGREE;
				case UP:
					return THREE_QUARTER__ROTATION_DEGREE;
				case DOWN:
					return QUARTER_ROTATION_DEGREE;
				default:
					return NULL_ROTATION_DEGREE;
			}
			case UP:
				switch (toDirection) {
					case LEFT:
						return THREE_QUARTER__ROTATION_DEGREE;
					case RIGHT:
						return QUARTER_ROTATION_DEGREE;
					case UP:
						return NULL_ROTATION_DEGREE;
					case DOWN:
						return HALF_ROTATION_DEGREE;
					default:
						return NULL_ROTATION_DEGREE;
				}
			case DOWN:
				switch (toDirection) {
					case LEFT:
						return QUARTER_ROTATION_DEGREE;
					case RIGHT:
						return THREE_QUARTER__ROTATION_DEGREE;
					case UP:
						return HALF_ROTATION_DEGREE;
					case DOWN:
						return NULL_ROTATION_DEGREE;
					default:
						return NULL_ROTATION_DEGREE;
				}
			default:
				return NULL_ROTATION_DEGREE;
		}
    }
			
}
