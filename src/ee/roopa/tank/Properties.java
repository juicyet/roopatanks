package ee.roopa.tank;

import android.graphics.Point;

public class Properties {
	
	public static int GAME_AREA_HEIGHT;
	public static int GAME_AREA_WIDTH;
	
	public static int GAMEOBJECT_FATNESS = 24;
	
	public static int ARROW_CONTROL_X;
	public static int ARROW_CONTROL_Y;
	public static int ARROW_CONTROL_HEIGHT = 100;
	public static int ARROW_CONTROL_WIDTH = 100;
	
	public static int SHOOT_BUTTON_X;
	public static int SHOOT_BUTTON_Y;
	public static int SHOOT_BUTTON_HEIGHT = 50;
	public static int SHOOT_BUTTON_WIDTH = 50;
	
	public static int GAME_OVER_TEXT_WITH =  GAMEOBJECT_FATNESS;
	public static int GAME_OVER_TEXT_HEIGHT =  GAMEOBJECT_FATNESS;
	
	public static int TURKEY_X_LOWER;
	public static int TURKEY_Y_LOWER;	
	public static int TURKEY_X_UPPER;
	public static int TURKEY_Y_UPPER;	
	public static int TURKEY_HEIGHT =  GAMEOBJECT_FATNESS;
	public static int TURKEY_WIDTH =  GAMEOBJECT_FATNESS;
		
	public static int ENEMIES_PER_LEVEL = 48;
	
	public static final int PADDING = 2;	
		
	// spawning locations for AI tanks
	public static Point SPAWNING_POINT_LEFT_UPPER;
	public static Point SPAWNING_POINT_LEFT_LOWER;
	public static Point SPAWNING_POINT_RIGHT_UPPER;
	public static Point SPAWNING_POINT_RIGHT_LOWER;
	
	// spawning locations for player tanks
	public static Point SPAWNING_POINT_TOP_LEFT;
	public static Point SPAWNING_POINT_TOP_RIGHT;	
	public static Point SPAWNING_POINT_BOTTOM_LEFT;
	public static Point SPAWNING_POINT_BOTTOM_RIGHT;
	
	//Specify an alpha value for the drawable. 0 means fully transparent, and 255 means fully opaque.
	public static int CONTROLS_OPACITY = 100;
	
	public static int TANK_WIDTH_NORMAL =  GAMEOBJECT_FATNESS;
	public static int TANK_HEIGHT_NORMAL =  GAMEOBJECT_FATNESS;	

	public static final int SHOOTING_INTERVAL_NORMAL = 12;
	public static final int BULLET_RADIUS_NORMAL = 3;
	
	public static final int TANK_SPEED_NORMAL = 2;
	
	public static final int BULLET_COOLDOWN_COUNTER = 25;
	
	public static int BULLET_SPEED = 5;
	
	//Constants for GameObject types
	public static String BRICK_CHAR = "T";
	public static String METAL_CHAR = "R";
	public static String BUSH_CHAR = "P";
	public static String WATER_CHAR = "V";
	public static String GRASS_CHAR = "M";
	public static String EMPTY_CHAR = "E";
	
	public static int mapWidth = 0;
	public static int mapHeight = 0;
}
