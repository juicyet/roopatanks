/**
 * 
 */
package ee.roopa.tank;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

/**
 * @author andrelv
 *
 */
public abstract class GameControlBase {
	private int x, y, h, w;
	private Drawable controlImage;
	
	public GameControlBase(Drawable controlImage, int x, int y, int h, int w) {		
		this.controlImage = controlImage;
		this.x = x;
		this.y = y;
		this.h = h;
		this.w = w;
	}
	
	public Drawable getControlImage() {
		return controlImage;
	}

	
	public void show(Canvas canvas) {
		this.controlImage.setBounds(this.x, this.y, this.x + this.w, this.y + this.h);
		controlImage.draw(canvas);
	}
	
}
