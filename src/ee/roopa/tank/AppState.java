package ee.roopa.tank;

public enum AppState {
	Starting,
	Running,
	Paused,
	GameOver,
	Terminated
}
