package ee.roopa.tank;


import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;

public class PowerUp extends GameObject 
{	

	private static final PowerUpType powerupType = PowerUpType.randomPowerup();
	private static final int drawable = powerupType.getDrawable();
	
	public PowerUp(Resources res, int x, int y, int width, int height) { 
		super((BitmapDrawable)res.getDrawable(drawable), x, y, width, height, 0);
	}
}