package ee.roopa.tank;

import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import roopa.ee.R;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;

public class MapReader {
	
	Resources resources;	
	
	public static List<TerrainObjectsWrapper> readMap(BufferedReader reader, Resources resources) {
		List<TerrainObjectsWrapper> terrainObjects = null;
		int y = 0;
		int x = 0;
		
		try {
			String line;
			String terrainChar;
			terrainObjects = new ArrayList<TerrainObjectsWrapper>();
			StringTokenizer tokenizer;
			while ((line = reader.readLine()) != null) {
				tokenizer = new StringTokenizer(line.trim(), ",");
				while (tokenizer.hasMoreTokens()) {
					terrainChar = tokenizer.nextToken().trim();
					terrainObjects.add(new TerrainObjectsWrapper(x, y, terrainChar));
					x++;
				}
				Properties.mapWidth = x;
				x = 0;
				y++;
				Log.d("LEVEL_TAG", line);
			}
			Properties.mapHeight = y;
		} catch (Exception e) {
			System.out.println("Exception while reading csv file: " + e);
		}
		return terrainObjects;
	}

}
