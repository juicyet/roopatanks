package ee.roopa.tank;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

import android.util.Log;

/**
 * @author andrelv
 *
 */
public class SocketConnection {

	private final String SERVER_IP = "192.168.1.73";
	private final int SERVER_PORT = 12345;
	
	InetAddress serverAddr;
	BufferedReader in;
	Socket socket;
			

	/**
	 * Creates a new socket connection
	 */
	public SocketConnection() {
		initSocket();
	}
	
	private void initSocket() {
		try {
			serverAddr = InetAddress.getByName(SERVER_IP);
			Log.d("TCP", "C: Connecting...");
			socket = new Socket(serverAddr, SERVER_PORT);
			
			in = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
		} catch (Exception e) {
			Log.e("TCP", "C: Error", e);
		}
	}
}
